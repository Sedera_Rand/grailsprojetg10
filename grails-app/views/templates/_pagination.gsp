<div class="card-footer clearfix">
    <ul class="pagination pagination-sm m-0 float-right">
        <g10:paginate total="${annonceCount}" params="${params}" controller="annonce"/>
    </ul>
</div>
