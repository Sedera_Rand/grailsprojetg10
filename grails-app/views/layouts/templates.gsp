<!doctype html>
<html lang="en" class="no-js">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <title>
        <g:layoutTitle default="Grails"/>
    </title>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <asset:link rel="icon" href="AdminLTELogo.png" type="image/x-ico"/>
    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome -->
    <asset:stylesheet rel="stylesheet" src="all.min.css"/>
    <!-- DataTables -->
    <asset:stylesheet rel="stylesheet" src="dataTables.bootstrap4.min.css"/>
    <asset:stylesheet rel="stylesheet" src="responsive.bootstrap4.min.css"/>
    <asset:stylesheet rel="stylesheet" src="buttons.bootstrap4.min.css"/>
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Tempusdominus Bootstrap 4 -->
    <asset:stylesheet rel="stylesheet" src="tempusdominus-bootstrap-4.min.css"/>
    <!-- iCheck -->
    <asset:stylesheet rel="stylesheet" src="icheck-bootstrap.min.css"/>
    <!-- JQVMap -->
    <asset:stylesheet rel="stylesheet" src="jqvmap.min.css"/>
    <!-- Theme style -->
    <asset:stylesheet rel="stylesheet" src="adminlte.min.css"/>
    <!-- overlayScrollbars -->
    <asset:stylesheet rel="stylesheet" src="OverlayScrollbars.min.css"/>
    <!-- Daterange picker -->
    <asset:stylesheet rel="stylesheet" src="daterangepicker.css"/>
    <!-- summernote -->
    <asset:stylesheet rel="stylesheet" src="summernote-bs4.min.css"/>
    <!-- Toast -->
    <asset:stylesheet href="toast.min.css" rel="stylesheet"/>

    <g:layoutHead/>
</head>
<body>

<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">
    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand navbar-white navbar-light">
        <!-- Left navbar links -->
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
            </li>
        </ul>
        <ul class="navbar-nav ml-auto">
            <li class="nav-item dropdown">
                <a class="nav-link" data-toggle="dropdown" href="#">
                    <i class="far fa-user"></i>
                </a>
                <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                    <a href="#" class="dropdown-item">
                        <!-- Message Start -->
                        <div class="media">
                            <div class="media-body">
                                <g:form controller="logout">
                                    <g:submitButton name="logout" class="btn btn-default" value="Se déconnecter" />
                                </g:form>
                            </div>
                        </div>
                        <!-- Message End -->
                    </a>
                </div>
            </li>
        </ul>

    </nav>
    <!-- /.navbar -->
    <!-- Main Sidebar Container -->
    <aside class="main-sidebar sidebar-dark-primary elevation-4">
        <!-- Brand Logo -->
        <a href="#" class="brand-link">
            <asset:image src="AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8"/>
            <span class="brand-text font-weight-light">Administration Lite </span>
        </a>

        <!-- Sidebar -->
        <div class="sidebar">
            <!-- Sidebar user panel (optional) -->
            <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                <div class="image">
                    <asset:image src="user3-128x128.jpg" class="img-circle elevation-2" alt="User Image"/>
                </div>
                <sec:ifLoggedIn>
                    <div class="info">
                        <a href="#" class="d-block"><sec:loggedInUserInfo field='username'/></a>
                    </div>
                </sec:ifLoggedIn>
            </div>

            <!-- Sidebar Menu -->
            <nav class="mt-2">
                <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                    <li class="nav-item menu-open">
                        <a href="${createLink(controller: 'annonce', action: 'index')}" class="nav-link ">
                            <i class="nav-icon fas fa-tachometer-alt"></i>
                            <p>
                                Annonces
                            </p>
                        </a>
                        <sec:ifAllGranted roles="ROLE_ADMIN">
                        <a href="${createLink(controller: 'user', action: 'index')}" class="nav-link">
                            <i class="nav-icon fas fa-user-alt"></i>
                            <p>
                                Utilisateurs
                            </p>
                        </a>
                        </sec:ifAllGranted>
                    </li>
                </ul>
            </nav>
            <!-- /.sidebar-menu -->
        </div>
        <!-- /.sidebar -->
    </aside>
    <div class="content-wrapper">
        <g:layoutBody/>
    </div>

</div>
</body>

<footer class="main-footer">
    <div class="float-right d-none d-sm-block">
        <b>Version</b> 3.1.0-rc
    </div>
    <strong>Copyright &copy; 2020 <a href="https://adminlte.io">Admin ETSENA ITU MBDS </a>.</strong> All rights reserved.
</footer>



<!-- jQuery -->
<asset:javascript src="jquery.min.js"/>
<!-- jQuery UI 1.11.4 -->
<asset:javascript src="jquery-ui.min.js"/>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->

<!-- Bootstrap 4 -->
<asset:javascript src="bootstrap.bundle.min.js"/>
<!-- ChartJS -->
<asset:javascript src="Chart.min.js"/>
<!-- Sparkline -->
<asset:javascript src="sparkline.js"/>

<!-- daterangepicker -->
<asset:javascript src="moment.min.js"/>
<asset:javascript src="daterangepicker.js"/>
<!-- Tempusdominus Bootstrap 4 -->
<asset:javascript src="tempusdominus-bootstrap-4.min.js"/>
<!-- Summernote -->
<asset:javascript src="summernote-bs4.min.js"/>
<!-- overlayScrollbars -->
<asset:javascript src="jquery.overlayScrollbars.min.js"/>
<!-- DataTables  & Plugins -->
<asset:javascript src="jquery.dataTables.min.js"/>
<asset:javascript src="dataTables.bootstrap4.min.js"/>
<asset:javascript src="dataTables.responsive.min.js"/>
<asset:javascript src="responsive.bootstrap4.min.js"/>
<asset:javascript src="dataTables.buttons.min.js"/>
<asset:javascript src="buttons.bootstrap4.min.js"/>
<asset:javascript src="jszip.min.js"/>
<asset:javascript src="pdfmake.min.js"/>
<asset:javascript src="vfs_fonts.js"/>
<asset:javascript src="buttons.html5.min.js"/>
<asset:javascript src="buttons.print.min.js"/>
<asset:javascript src="buttons.colVis.min.js"/>
<!-- AdminLTE App -->
<asset:javascript src="adminlte.js"/>
<!-- AdminLTE for demo purposes -->
<asset:javascript src="demo.js"/>
<asset:javascript src="toast.min.js"/>

<g:javascript>
    $(function () {
        $('.btn-remove').on('click', function (event){
            event.preventDefault();
            var id = $(this).data('id');
            $('#btn-remove').attr('data-id', id);
        });

        $('#delete').on('click', function (event) {
            event.preventDefault();
            var id = $('#btn-remove').data('id');

            $.ajax({
                url: "http://localhost:8081/annonce/delete/" + id,
                type: "DELETE",
                success: function (data) {
                    $('#btn-remove').modal('toggle');
                    window.location.reload(false);
                },
                error: function () {
                    error();
                }
            });
        });
    });
</g:javascript>


<g:javascript>
    $(function () {
        $('.btn-remove-user').on('click', function (event){
            event.preventDefault();
            var id = $(this).data('id');
            $('#btn-remove-user').attr('data-id', id);
        });

        $('#delete-user').on('click', function (event) {
            event.preventDefault();
            var id = $('#btn-remove-user').data('id');

            $.ajax({
                url: "http://localhost:8081/user/delete/" + id,
                type: "DELETE",
                success: function (data) {
                    $('#btn-remove-user').modal('toggle');
                    window.location.reload(false);
                },
                error: function () {
                    error();
                }
            });
        });
    });
</g:javascript>

</body>
</html>