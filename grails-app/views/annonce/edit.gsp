<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="templates"/>
    <g:set var="entityName" value="${message(code: 'annonce.label', default: 'Annonce')}"/>
    <title><g:message code="default.edit.label" args="[entityName]"/></title>
</head>

<body>
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#"><g:link class="index" action="index">Liste</g:link></a></li>
                        <li class="breadcrumb-item active">Edition</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <div class="col-md-12">
    <div class="card card-info">
        <div class="card-header">
            <h3 class="card-title">Editer Annonce</h3>
        </div>
        <g:if test="${flash.message}">
            <div class="message" role="status">${flash.message}</div>
        </g:if>
        <g:hasErrors bean="${this.annonce}">
            <ul class="errors" role="alert">
                <g:eachError bean="${this.annonce}" var="error">
                    <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
                </g:eachError>
            </ul>
        </g:hasErrors>
    <!-- form start -->
        <form action="/annonce/update/${annonce.id}" method="post" enctype="multipart/form-data">
            <div class="card-body">
                <g:hiddenField name="version" value="${this.annonce?.version}" />
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Titre</label>
                    <div class="col-sm-10">
                        <g:textField name="title" value="${fieldValue(bean: annonce, field: "title")}" type="text" class="form-control" placeholder="${params?.title}"  />
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Description</label>
                    <div class="col-sm-10">
                        <g:textField name="description" class="form-control" value="${fieldValue(bean: annonce, field: "description")}" type="text" placeholder="Description"  />
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 control-label">Prix</label>
                    <div class="col-sm-10">
                        <g:field name="price" class="form-control" value="${fieldValue(bean: annonce, field: "price")}" type="number" placeholder="Price"  />
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Auteur</label>
                    <div class="col-sm-10">
                        <g:select name="author.username" value="${fieldValue(bean: annonce, field: "author.username")}" type="select" class="form-control" from="${com.mbds.grails.User.list()}" />
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 control-label">Illustrations</label>
                    <div class="col-sm-3">
                        <g:hiddenField name="id" value="${this.annonce?.id}" />
                        <g:hiddenField name="version" value="${this.annonce?.version}" />
                        <input type="file" class="custom-file-input" id="featuredImageFile" name="featuredImageFile"/>
                        <label class="custom-file-label" for="featuredImageFile">Change Illustration</label>
                    </div>
                </div>

            </div>

            <!-- /.card-body -->
            <div class="card-footer">
                <g:submitButton name="save" class="btn btn-warning" value="${message(code: 'default.button.update.label', default: 'Update')}" />
            <button type="button" class="snack btn btn-default float-right"><a href="${createLink(controller: 'annonce', action: 'index')}">Cancel</a></button>
            </div>
            <!-- /.card-footer -->
        </form>
    </div>
    <!-- /.card -->

</div>

</body>
</html>
