<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="templates" />
        <g:set var="entityName" value="${message(code: 'annonce.label', default: 'Annonce')}" />
        <title><g:message code="default.create.label" args="[entityName]" /></title>
    </head>
    <body>
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#"><g:link class="index" action="index">Liste</g:link></a></li>
                        <li class="breadcrumb-item active">Ajout</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <div class="col-md-12">
        <div class="card card-info">
            <div class="card-header">
                <h3 class="card-title">Nouvelle annonce</h3>
            </div>
            <g:if test="${flash.message}">
                <div class="message" role="status">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${this.product}">
                <ul class="errors" role="alert">
                    <g:eachError bean="${this.product}" var="error">
                        <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
                    </g:eachError>
                </ul>
            </g:hasErrors>
            <!-- form start -->
            <g:form resource="${this.annonce}" action="save" method="POST" enctype="multipart/form-data">
                <div class="card-body">
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Titre</label>
                        <div class="col-sm-10">
                            <g:textField name="title" value="${params?.title}" type="text" class="form-control" placeholder="Title" required="required"/>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Déscription</label>
                        <div class="col-sm-10">
                            <g:textArea name="description" class="form-control" value="${params?.description}" type="text" placeholder="Description"  />
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 control-label">Illustration</label>
                        <div class="col-sm-10">
                            <input type="file" class="custom-file-input" id="featuredImageFile" name="featuredImageFile"/>
                            <label class="custom-file-label" for="featuredImageFile">Choisir une image</label>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-2 control-label">Prix</label>
                        <div class="col-sm-10">
                            <g:textField name="price" class="form-control" value="${params?.price}" type="textArea" placeholder="Prix" required="required"/>
                        </div>
                    </div>
                </div>

                <!-- /.card-body -->
                <div class="card-footer">
                    <g:submitButton name="create"  class="btn btn-primary" value="${message(code: 'default.button.create.label', default: 'Créer')}" />
                    <button type="button" class="snack btn btn-default float-right"><a href="${createLink(controller: 'annonce', action: 'index')}">Annuler</a></button>
                </div>
                <!-- /.card-footer -->
            </g:form>
        </div>
        <!-- /.card -->

    </div>

    </body>
</html>
