<%--
  Created by IntelliJ IDEA.
  User: Dev
  Date: 25/03/2021
  Time: 12:18
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="templates" />
    <g:set var="entityName" value="${message(code: 'annonce.label', default: 'Annonce')}" />
    <title><g:message code="default.list.label" args="[entityName]" /></title>
</head>

<body class="hold-transition sidebar-mini">
<div class="wrapper">
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Liste</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="${createLink(controller: "annonce", action: "index")}">Accueil</a></li>
                        <li class="breadcrumb-item active"></li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <g:if test="${Annonce_list.size > 0}">
                                <li class="breadcrumb-item"><a href="#">"${Annonce_list.size}" Resultat recherche pour "${searchText}"</a></li>
                            </g:if>
                            <g:else>
                                <li class="breadcrumb-item"><a href="#">Pas d'articles trouvés</a></li>
                            </g:else>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body table-responsive p-0">
                            <g:if test="${flash.message}">
                                <div class="message" role="status">${flash.message}</div>
                            </g:if>
                            <table id="example1" class="table table-hover text-nowrap">
                                <thead>
                                <tr>
                                    <th>Title</th>
                                    <th>Description</th>
                                    <th>Price</th>
                                    <th>Illustration</th>
                                    <th>Author</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                <g:each in="${Annonce_list}" var="annonceList">
                                    <tr>
                                        <td><a href="${createLink(controller: 'annonce', action: 'show', params: [id: annonceList.id])}">${annonceList.title}</a></td>
                                        <td>${annonceList.description}</td>
                                        <td>${annonceList.price} €</td>
                                        <td>
                                            <ul>
                                                <g:each in="${annonceList.illustrations}" var="illustration">
                                                    <img src="assets/illustrations/${illustration.filename}" widht='80px' height='80px'>
                                                </g:each>
                                            </ul>
                                        </td>
                                        <td><a href="/projet/user/show/${annonceList.author.id}">${annonceList.author.username}</a></td>

                                        <td>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <a href="${createLink(controller: 'annonce', action: 'edit', params: [id: annonceList.id])}">
                                                        <button type="button" class="btn btn-primary btn-block btn-sm"><i class="fa fa-edit"></i>Edit</button>
                                                    </a>
                                                </div>
                                                <div class="col-md-4">
                                                    <a href="#">
                                                        <button type="button" class="btn btn-danger btn-block btn-sm btn-remove" data-id="${annonceList.id}" data-toggle="modal" data-target="#btn-remove">
                                                            <i class="fa fa-trash"></i>
                                                            Delete
                                                        </button>
                                                    </a>
                                                </div>

                                            </div>
                                        </td>
                                    </tr>
                                </g:each>
                                </tbody>
                            </table>
                            %{--                                <div class="card-footer clearfix">--}%
                            %{--                                    <ul class="pagination pagination-sm m-0 float-right">--}%
                            %{--                                        <g:paginate total="${annonceCount}" action="index" controller="annonce" params="${params}"/>--}%
                            %{--                                    </ul>--}%
                            %{--                                </div>--}%
                            %{--<g:render template="/templates/pagination"/>--}%
                        </div>

                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </div>
    </section>
</div>
</body>
</html>