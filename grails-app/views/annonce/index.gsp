<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="templates" />
    <g:set var="entityName" value="${message(code: 'annonce.label', default: 'Annonce')}" />
    <title><g:message code="default.list.label" args="[entityName]" /></title>
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Liste</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="${createLink(controller: "annonce", action: "index")}">Accueil</a></li>
                        <li class="breadcrumb-item active"></li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Annonces</h3>
                            <div class="card-tools">
                                <form action="${createLink(controller: "annonce", action: "results")}" method="get">
                                    <div class="input-group input-group-sm" style="width: 150px;">
                                        <g:form action="results" class="form-outline">
                                            <g:textField type="search" name="searchText" class="form-control float-right" placeholder="Recherche"/>
                                            <div class="input-group-append">
                                                <g:submitButton name="search" value="Valider" class="btn btn-default">
                                                    <i class="fas fa-search"></i>
                                                </g:submitButton>
                                            </div>
                                        </g:form>
                                         <sec:ifNotGranted roles="ROLE_USER">
                                            <div class="panel-options" align="right">
                                                <div class="col-md-12">
                                                    <a href="#" data-rel="collapse"><g:link class="create" action="create"><i class="fas fa-plus"></i></g:link></a>
                                                </div>
                                            </div>
                                        </sec:ifNotGranted>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body table-responsive p-0">
                            <g:if test="${flash.message}">
                                <div class="message" role="status">${flash.message}</div>
                            </g:if>
                            <table id="example1" class="table table-hover text-nowrap">
                                <thead>
                                <tr>
                                    <th>Titre</th>
                                    <th>Déscription</th>
                                    <th>Prix</th>
                                    <th>Illustration</th>
                                    <th>Auteur</th>
                                    <sec:ifNotGranted roles="ROLE_USER">
                                        <th>Actions</th>
                                    </sec:ifNotGranted>
                                </tr>
                                </thead>
                                <tbody>
                                <g:each in="${annonceList}" var="annonce">
                                    <tr>
                                        <td><a href="${createLink(controller: 'annonce', action: 'show', params: [id: annonce.id])}">${annonce.title}</a></td>
                                        <td>${annonce.description}</td>
                                        <td>${annonce.price} €</td>
                                        <td>
                                            <ul>
                                                <g:each in="${annonce.illustrations}" var="illustration">
                                                    <img src="assets/illustrations/${illustration.filename}" widht='80px' height='80px'>
                                                </g:each>
                                            </ul>
                                        </td>
                                        <td><a href="/projet/user/show/${annonce.author.id}">${annonce.author.username}</a></td>
                                        <sec:ifNotGranted roles="ROLE_USER">
                                            <td>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <a href="${createLink(controller: 'annonce', action: 'edit', params: [id: annonce.id])}">
                                                            <button type="button" class="btn btn-primary btn-block btn-sm"><i class="fa fa-edit"></i>Modifier</button>
                                                        </a>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <a href="#">
                                                            <button type="button" class="btn btn-danger btn-block btn-sm btn-remove" data-id="${annonce.id}" data-toggle="modal" data-target="#btn-remove">
                                                                <i class="fa fa-trash"></i>
                                                                Supprimer
                                                            </button>
                                                        </a>
                                                    </div>
                                                </div>
                                            </td>
                                        </sec:ifNotGranted>
                                    </tr>
                                </g:each>
                                </tbody>
                            </table>
                            <g:render template="/templates/pagination"/>
                        </div>

                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </div>
    </section>
</div>

<div class="modal fade" id="btn-remove" data-id="">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                Vous-êtes sur de supprimer cet annonce?
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-warning" id="delete">Supprimer</button>
                <button type="button" data-dismiss="modal" class="btn btn-dark">Annuler</button>
            </div>
        </div>
    </div>
</div>

</body>
</html>
