<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="templates" />
        <g:set var="entityName" value="${message(code: 'annonce.label', default: 'Annonce')}" />
        <title><g:message code="default.show.label" args="[entityName]" /></title>
    </head>
    <body>
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#"><g:link class="index" action="index">Liste des annonces</g:link></a></li>
                        <li class="breadcrumb-item active">Details</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->

        <div class="col-md-12">
            <div class="card card-info">
                <div class="card-header">
                    <h3 class="card-title">Details annonce</h3>
                    <div class="panel-options" align="right">
                        <div class="col-md-12">
                            <sec:ifNotGranted roles="ROLE_USER">
                                <g:form resource="${this.annonce}" method="DELETE">
                                    <input class="btn-info delete" type="submit" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
                                </g:form>
                            </sec:ifNotGranted>
                        </div>
                    </div>
                </div>
                <g:if test="${flash.message}">
                    <div class="message" role="status">${flash.message}</div>
                </g:if>
                <g:hasErrors bean="${this.annonce}">
                    <ul class="errors" role="alert">
                        <g:eachError bean="${this.annonce}" var="error">
                            <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
                        </g:eachError>
                    </ul>
                </g:hasErrors>
            <!-- form start -->
                <g:form resource="${this.annonce}" method="PUT" >
                    <div class="card-body">
                        <g:hiddenField name="version" value="${this.annonce?.version}" />
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Titre</label>
                            <div class="col-sm-10">
                                <f:display bean="annonce" property="title"/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Description</label>
                            <div class="col-sm-10">
                                <f:display bean="annonce" property="description"/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 control-label">Prix</label>
                            <div class="col-sm-10">
                                <f:display bean="annonce" property="price"/> €
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 control-label">Illustrations</label>
                            <g:each in="${annonce.illustrations}" var="illustration">
                                <g:img dir="assets" file="/illustrations/${illustration.filename}" width="80px" heigth="80px" />
                            </g:each>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Auteur</label>
                            <f:display bean="annonce" property="author.username"/>
                        </div>

                    </div>

                    <!-- /.card-body -->

                        <div class="card-footer">
                    <sec:ifNotGranted roles="ROLE_USER">
                            <g:link class="edit" action="edit" resource="${this.annonce}">
                                <button  type="button" class="btn btn-primary">Modifier</button>
                            </g:link>
                    </sec:ifNotGranted>
                            <button type="button" class="snack btn btn-default float-right"><a href="${createLink(controller: 'annonce', action: 'index')}">Retour</a></button>
                        </div>
                    <!-- /.card-footer -->
                </g:form>
            </div>
            <!-- /.card -->
        </div>

    </section>
    </body>
</html>
