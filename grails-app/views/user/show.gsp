<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="templates" />
        <g:set var="entityName" value="${message(code: 'user.label', default: 'User')}" />
        <title><g:message code="default.show.label" args="[entityName]" /></title>
    </head>
    <body>
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#"><g:link class="index" action="index">Liste</g:link></a></li>
                        <li class="breadcrumb-item active">Show</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <div class="col-md-12">
        <div class="card card-info">
            <div class="card-header">
                <h3 class="card-title">Détail de l'utilisateur</h3>
                <div class="panel-options" align="right">
                    <div class="col-md-12">
                        <g:form resource="${this.edit}" method="DELETE">
                            <fieldset class="buttons">
                                <g:link class="edit" action="edit" resource="${this.user}"><g:message code="default.button.edit.label" default="Modifier" /></g:link>
                                <input class="delete" type="submit" value="${message(code: 'default.button.delete.label', default: 'Supprimer')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Vous êtes sure?')}');" />
                            </fieldset>
                        </g:form>
                    </div>
                </div>
            </div>
            <g:if test="${flash.message}">
                <div class="message" role="status">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${this.user}">
                <ul class="errors" role="alert">
                    <g:eachError bean="${this.user}" var="error">
                        <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
                    </g:eachError>
                </ul>
            </g:hasErrors>
        <!-- form start -->
            <g:form resource="${this.user}" method="PUT" >
                <div class="card-body">
                    <g:hiddenField name="version" value="${this.user?.version}" />
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Nom</label>
                        <div class="col-sm-10">
                            <f:display bean="user" property="username"/>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Mot de passe expiré</label>
                        <div class="col-sm-10">
                            ${this.user.passwordExpired ? 'Oui' : 'Non'}
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Statut du compte</label>
                        <div class="col-sm-10">
                            ${this.user.accountLocked ? 'Actif' : 'Bloqué'}
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Compte expiré</label>
                        <div class="col-sm-10">
                            ${this.user.accountExpired ? 'Oui' : 'Non'}
                        </div>
                    </div>
                </div>

                <!-- /.card-body -->
                <div class="card-footer">

                </div>
                <!-- /.card-footer -->
            </g:form>
        </div>
        <!-- /.card -->
    </div>

    </body>
</html>
