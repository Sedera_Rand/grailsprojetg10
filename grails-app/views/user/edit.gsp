<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="templates" />
        <g:set var="entityName" value="${message(code: 'user.label', default: 'User')}" />
        <title><g:message code="default.edit.label" args="[entityName]" /></title>
    </head>
    <body>
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#"><g:link class="index" action="index">Liste</g:link></a></li>
                        <li class="breadcrumb-item active">Modifier</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <div class="col-md-12">
        <div class="card card-info">
            <div class="card-header">
                <h3 class="card-title">Edition utilisateur</h3>
            </div>
            <g:if test="${flash.message}">
                <div class="message" role="status">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${this.annonce}">
                <ul class="errors" role="alert">
                    <g:eachError bean="${this.annonce}" var="error">
                        <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
                    </g:eachError>
                </ul>
            </g:hasErrors>
        <!-- form start -->
            <g:form resource="${this.user}" method="PUT" >
                <div class="card-body">
                    <g:hiddenField name="version" value="${this.user?.version}" />
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Nom d'utilisateur</label>
                        <div class="col-sm-10">
                            <g:textField name="username" value="${fieldValue(bean: user, field: "username")}" type="text" class="form-control" placeholder="${params?.username}"  />
                        </div>
                    </div>
            %{--  <div class="form-group row">
                  <label class="col-sm-2 col-form-label">Mot de passe</label>
                  <div class="col-sm-10">
                        <g:passwordField name="password1" value="${fieldValue(bean: user, field: "password")}" type="password" class="form-control" placeholder="Mot de passe" />
                    </div>
                </div>   --}%
            </div>

            <!-- /.card-body -->
            <div class="card-footer">
                <g:submitButton name="save" class="btn btn-warning" value="${message(code: 'default.button.update.label', default: 'Mettre à jour')}" />
                <button type="button" class="snack btn btn-default float-right"><a href="${createLink(controller: 'user', action: 'index')}">Annuler</a></button>
            </div>
            <!-- /.card-footer -->
        </g:form>
    </div>
    <!-- /.card -->

</div>

</body>
</html>
