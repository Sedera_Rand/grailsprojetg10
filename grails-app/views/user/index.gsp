<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="templates" />
        <g:set var="entityName" value="${message(code: 'user.label', default: 'User')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Administration</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Accueil</a></li>
                        <li class="breadcrumb-item active"></li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Liste des utilisateurs</h3>
                            <div class="card-tools">
                                <div class="input-group input-group-sm" style="width: 150px;">
                                    <input type="text" name="table_search" class="form-control float-right">
                                    <div class="input-group-append">
                                        <button type="submit" class="btn btn-default">
                                            <i class="fas fa-search"></i>
                                        </button>
                                    </div>
                                    <div class="panel-options" align="right">
                                        <div class="col-md-12">
                                            <a href="#" data-rel="collapse"><g:link class="create" action="create"><i class="fas fa-plus"></i></g:link></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body table-responsive p-0">
                            <g:if test="${flash.message}">
                                <div class="message" role="status">${flash.message}</div>
                            </g:if>
                            <table class="table table-hover text-nowrap">
                                <thead>
                                <tr>
                                    <th>Nom d'utilisateur</th>
                                    <th>Annonces</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                <g:each in="${UserList}" var="user">
                                    <tr>
                                        <td>${user.username}</td>
                                        <td>
                                            <g:each in="${user.annonces}" var="annonce">
                                                <ul><a href="${createLink(controller: 'user', action: 'show', params: [id: user.id])}">${annonce.title}</a></ul>
                                            </g:each>
                                        </td>
                                        <td>
                                            <div class="row" >
                                                <div class="col-md-3">
                                                    <a href="${createLink(controller: 'user', action: 'edit', params: [id: user.id])}">
                                                        <button type="button" class="btn btn-primary btn-block btn-sm"><i class="fa fa-edit"></i>Modifier</button>
                                                    </a>
                                                </div>
                                                <div class="col-md-3">
                                                    <a href="#">
                                                        <button type="button" class="btn btn-danger btn-block btn-sm btn-remove-user" data-id="${user.id}" data-toggle="modal" data-target="#btn-remove-user">
                                                            <i class="fa fa-trash"></i>
                                                            Supprimer
                                                        </button>
                                                    </a>
                                                </div>

                                            </div>
                                        </td>
                                    </tr>
                                </g:each>
                                </tbody>
                            </table>
                        <g:render template="/templates/paginationUser"/>
                        </div>

                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </div>
    </section>
</div>

<div class="modal fade" id="btn-remove-user" data-id="">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                Vous-êtes sur de supprimer cet utilisateur?
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-warning" id="delete-user">Supprimer</button>
                <button type="button" data-dismiss="modal" class="btn btn-dark">Annuler</button>
            </div>
        </div>
    </div>
</div>

</body>
</html>