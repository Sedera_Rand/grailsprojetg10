<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="templates" />
    <g:set var="entityName" value="${message(code: 'annonce.label', default: 'Annonce')}" />
    <title><g:message code="default.create.label" args="[entityName]" /></title>
</head>
<body>
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1></h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#"><g:link class="index" action="index">Liste</g:link></a></li>
                    <li class="breadcrumb-item active">Ajout</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<div class="col-md-12">
    <div class="card card-info">
        <div class="card-header">
            <h3 class="card-title">Ajouter un utilisateur</h3>
        </div>
        <g:if test="${flash.message}">
            <div class="message" role="status">${flash.message}</div>
        </g:if>
        <g:hasErrors bean="${this.user}">
            <ul class="errors" role="alert">
                <g:eachError bean="${this.user}" var="error">
                    <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
                </g:eachError>
            </ul>
        </g:hasErrors>
    <!-- form start -->
        <g:form resource="${this.user}" action="save" onSubmit="checkPassword(this)" method="POST" enctype="multipart/form-data">
            <div class="card-body">
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Nom utilisateur</label>
                    <div class="col-sm-10">
                        <g:textField name="username" class="form-control" value="${params?.username}" type="text" placeholder="Nom"  />
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Rôle</label>
                    <div class="col-sm-10">
                        <g:select name="authrority" optionKey="id" type="select" class="form-control" from="${com.mbds.grails.Role.list()}" noSelection="['':'-Choisir Rôle-']" />
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Mot de passe</label>
                    <div class="col-sm-10">
                        <g:passwordField name="password" id="mdp1" value="${params?.password}" type="password" class="form-control" placeholder="Mot de passe" />
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Confirmer le mot de passe</label>
                    <div class="col-sm-10">
                        <g:passwordField name="password2" id="mdp2" value="${params?.password}" type="password" class="form-control" placeholder="Mot de passe" />
                        <span id="checkP" class="" ></span>
                    </div>
                </div>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
                <g:submitButton name="create"  class="btn btn-primary" value="${message(code: 'default.button.create.label', default: 'Create')}" />
                <button type="button" class="snack btn btn-default float-right"><a href="${createLink(controller: 'user', action: 'index')}">Retour</a></button>

            </div>
            <!-- /.card-footer -->
        </g:form>
    </div>
    <!-- /.card -->
    <g:javascript>
        let pass1= document.querySelector('#mdp1')
        let pass2= document.querySelector('#mdp2')
        let result= document.querySelector('#checkP')
        function checkPassword(form) {
            result.innerText = pass1.value == pass2.value ? 'Correct' : 'non correct';
        }
        pass1.addEventListener('keyup', () =>{
            if (pass2.value.length !=0) checkPassword();
        })

        pass2.addEventListener('keyup', checkPassword);
    </g:javascript>

</div>

</body>
</html>
