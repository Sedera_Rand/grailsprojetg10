package com.mbds.grails

class Illustration {

    // On va utiliser le filename pour construire deux chemins
    // URL = annonces.illustrations.url -> Adresse externe + chemin vers les images + filename
    // Path = annonces.illustrations.path -> Adresse interne + chemin vers les images + filename
    String filename
    String url

    static belongsTo = [annonce: Annonce]

    static constraints = {
        filename    nullable: false, blank: false
        url    nullable: true
    }
}
