package com.mbds.grails

import grails.config.Config
import groovy.transform.CompileStatic
import grails.core.support.GrailsConfigurationAware

@SuppressWarnings('GrailsStatelessService')
@CompileStatic
class UploadImageService implements GrailsConfigurationAware {

    String path
    String url
    IllustrationService illustrationService

    @Override
    void setConfiguration(Config co) {
        path = co.getRequiredProperty('annonces.illustrations.path')
        url = co.getRequiredProperty('annonces.illustrations.url')
    }

    @SuppressWarnings('JavaIoPackageAccess')
    Illustration uploadFeatureImage(FeaturedImageCommand command) {
        String filename = command.featuredImageFile.getOriginalFilename()
        String folderPath = "${path}/illustrations"
        File folder = new File(folderPath)

        if (!folder.exists()) {
            folder.mkdirs()
        }
        String path = "${folderPath}/${filename}"
        File file = new File(path).getAbsoluteFile()

        command.featuredImageFile.transferTo(file)
        String featuredImageUrl = "${url}/illustrations/${filename}"

        return new Illustration([filename: filename, url: featuredImageUrl])
    }
}
