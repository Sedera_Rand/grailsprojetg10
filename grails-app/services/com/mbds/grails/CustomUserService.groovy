package com.mbds.grails

class CustomUserService {
    void patchUser(User user, data) {
        if (data.getAt("username"))
            user.setUsername(data.getAt("username"))

        if (data.getAt("password"))
            user.setPassword(data.getAt("password"))

        user.save(flush: true)
    }

    void putUser(User user, data) {
        user.setUsername(data.getAt("username"))
        user.setPassword(data.getAt("password"))
        user.save(flush: true)
    }
}
