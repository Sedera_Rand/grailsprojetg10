package com.mbds.grails

class CustomAnnonceService {
    void patchAnnonce(Annonce annonce, data) {
        if (data.getAt("title")) {
            annonce.setTitle(data.getAt("title"))
        }
        if (data.getAt("description")) {
            annonce.setDescription(data.getAt("description"))
        }
        if (data.getAt("price")) {
            annonce.setPrice(data.getAt("price"))
        }

        annonce.save(flush: true)
    }

    void putAnnonce(Annonce annonce, data) {
        annonce.setTitle(data.getAt("title"))
        annonce.setDescription(data.getAt("description"))
        annonce.setPrice(data.getAt("price"))
        annonce.setLastUpdated(new Date())
        annonce.save(flush: true)
    }
}
