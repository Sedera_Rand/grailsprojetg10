package com.mbds.grails

import grails.converters.JSON
import grails.converters.XML
import grails.plugin.springsecurity.annotation.Secured

import javax.servlet.http.HttpServletResponse

@Secured('ROLE_ADMIN')
class ApiController {

    def springSecurityService
    AnnonceService annonceService
    UserService userService

    CustomAnnonceService customAnnonceService
    CustomUserService customUserService

//    GET / PUT / PATCH / DELETE
//    url : localhost:8081/projet/api/annonce(s)/{id}
    def annonce() {
        switch (request.getMethod()) {
            case "GET":
                checkId(params.id)
                Annonce annonce = annonceService.get(params.id)
                checkObject(annonce)
                serializeData(annonce, request.getHeader("Accept"))
                break
            case "PUT":
                checkId(params.id)
                Annonce annonce = annonceService.get(params.id)
                def data = request.getJSON()

                if (!data.getAt("title") || !data.getAt("description") || !data.getAt("price"))
                    return response.status = HttpServletResponse.SC_BAD_REQUEST

                customAnnonceService.putAnnonce(annonce, data)
                return response.status = HttpServletResponse.SC_NO_CONTENT
                break
            case "PATCH":
                checkId(params.id)
                Annonce annonce = annonceService.get(params.id)
                checkObject(annonce)
                def data = request.getJSON()

                customAnnonceService.patchAnnonce(annonce, data)
                return response.status = HttpServletResponse.SC_OK
                break
            case "DELETE":
                checkId(params.id)
                Annonce annonce = annonceService.get(params.id)
                checkObject(annonce)
                annonceService.delete(annonce.getId())
                return response.status = HttpServletResponse.SC_OK
                break
            default:
                return response.status = HttpServletResponse.SC_METHOD_NOT_ALLOWED
                break
        }
        return response.status = HttpServletResponse.SC_NOT_ACCEPTABLE
    }

//    GET / POST
    def annonces() {
        switch (request.getMethod()) {
            case "GET":
                List<Annonce> annoceList = Annonce.createCriteria().list(params, {order('id', 'desc')})
                serializeData(annoceList, request.getHeader("Accept"))
                break
            case "POST":
                def data = request.getJSON()
                Annonce annonce = new Annonce(
                        title: data.getAt("title"),
                        description: data.getAt("description"),
                        price: data.getAt("price"),
                )
                User currentUser = springSecurityService.getCurrentUser()
                currentUser.addToAnnonces(annonce)
                currentUser.save(flush: true, failOnError: true)
                return response.status = HttpServletResponse.SC_CREATED
                break
            default:
                return response.status = HttpServletResponse.SC_METHOD_NOT_ALLOWED
                break
        }

        return response.status = HttpServletResponse.SC_NOT_ACCEPTABLE
    }

//    GET / PUT / PATCH / DELETE
    def user() {
        switch (request.getMethod()) {
            case "GET":
                checkId(params.id)
                User user = userService.get(params.id)
                checkObject(user)
                serializeData(user, request.getHeader("Accept"))
                break
            case "PUT":
                checkId(params.id)
                User user = userService.get(params.id)
                def data = request.getJSON()

                if (!data.getAt("username") || !data.getAt("password"))
                    return response.status = HttpServletResponse.SC_BAD_REQUEST

                customUserService.putUser(user, data)
                return response.status = HttpServletResponse.SC_NO_CONTENT
                break
            case "PATCH":
                checkId(params.id)
                User user = userService.get(params.id)
                checkObject(user)
                def data = request.getJSON()

                customUserService.patchUser(user, data)
                return response.status = HttpServletResponse.SC_NO_CONTENT
                break
            case "DELETE":
                checkId(params.id)
                User user = userService.get(params.id)
                checkObject(user)
                UserRole.removeAll(user)
                userService.delete(params.id)
                return response.status = HttpServletResponse.SC_OK
                break
            default:
                return response.status = HttpServletResponse.SC_METHOD_NOT_ALLOWED
                break
        }
    }

//    GET / POST
    def users() {
        switch (request.getMethod()) {
            case "GET":
                def userList = User.list()
                serializeData(userList, request.getHeader("Accept"))
                break
            case "POST":
                def data = request.getJSON()
                User user = new User(
                        username: data.getAt("username"),
                        password: data.getAt("password")
                ).save()

                UserRole.create(user, getRole("ROLE_USER"), true)
                return response.status = HttpServletResponse.SC_CREATED
                break
            default:
                return response.status = HttpServletResponse.SC_METHOD_NOT_ALLOWED
                break
        }
        return response.status = HttpServletResponse.SC_NOT_ACCEPTABLE
    }

    def serializeData(object, format)
    {
        switch (format)
        {
            case 'json':
            case 'application/json':
            case 'text/json':
                render object as JSON
                break
            case 'xml':
            case 'application/xml':
            case 'text/xml':
                render object as XML
                break
            default:
                render object as XML
                break
        }
    }

    User getCurrentUser() {
        return springSecurityService.getCurrentUser()
    }

    Role getRole(String role) {
        return Role.findByAuthority(role.toUpperCase())
    }

    def checkId(id) {
        if (!id)
            return response.status = HttpServletResponse.SC_BAD_REQUEST
        return true
    }

    def checkObject(object) {
        if (!object)
            return response.status = HttpServletResponse.SC_NOT_FOUND
        return true
    }
}
