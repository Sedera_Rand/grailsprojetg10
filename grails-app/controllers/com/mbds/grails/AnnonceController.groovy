package com.mbds.grails

import grails.converters.JSON
import grails.plugin.springsecurity.annotation.Secured
import grails.validation.ValidationException
import org.springframework.web.multipart.MultipartFile

import static org.springframework.http.HttpStatus.*


class AnnonceController {

    AnnonceService annonceService
    UserService userService
    def springSecurityService
    UploadImageService uploadImageService

    @Secured(['ROLE_ADMIN','ROLE_MODO', 'ROLE_USER'])
    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        def list = Annonce.createCriteria().list(params, {order('id', 'desc')})
        respond list, model:[annonceCount: annonceService.count(),
                             annonceList : list,
                             baseUrl: grailsApplication.config.annonces.illustrations.url,
                             authorList: userService.list()
        ]
    }


    @Secured(['ROLE_ADMIN','ROLE_MODO','ROLE_USER'])
    def show(Long id) {
        respond annonceService.get(id), model: [urlImage: grailsApplication.config.annonces.illustrations.url, ]
    }

    @Secured(['ROLE_ADMIN','ROLE_MODO'])
    def create() {
        [annonce: new Annonce(), featuredImage: new FeaturedImageCommand()]
    }

    @Secured(['ROLE_ADMIN','ROLE_MODO'])
    def save(Annonce annonce, FeaturedImageCommand cmd) {

        User currentUser = springSecurityService.getCurrentUser()
        if (annonce == null) {
            notFound()
            return
        }

        try {
            if (cmd && cmd.featuredImageFile.getOriginalFilename()) {
                annonce.addToIllustrations(uploadImageService.uploadFeatureImage(cmd))
            }

            currentUser.addToAnnonces(annonce)
            currentUser.save(flush: true)
        } catch (ValidationException e) {
            respond annonce.errors, view:'create'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'annonce.label', default: 'Annonce'), annonce.id])
                redirect annonce
            }
            '*' { respond annonce, [status: CREATED] }
        }
    }

    @Secured(['ROLE_ADMIN','ROLE_MODO'])
    def edit(Long id) {
        Annonce annonce = annonceService.get(id)
        [annonce: annonce]
    }

    @Secured(['ROLE_ADMIN','ROLE_MODO'])
    def update(Annonce annonce, FeaturedImageCommand cmd) {
        if (annonce == null) {
            notFound()
            return
        }

        if (cmd && cmd.featuredImageFile.getOriginalFilename()) {
            annonce.addToIllustrations(uploadImageService.uploadFeatureImage(cmd))
        }

        try {
            annonceService.save(annonce)
        } catch (ValidationException e) {
            respond annonce.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'annonce.label', default: 'Annonce'), annonce.id])
                redirect annonce
            }
            '*'{ respond annonce, [status: OK] }
        }
    }

    @Secured(['ROLE_ADMIN','ROLE_MODO'])
    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }

        annonceService.delete(id)
        Map results = [
                status: 200
        ]
        render results as JSON
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'annonce.label', default: 'Annonce'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }

    @Secured(['ROLE_ADMIN','ROLE_MODO', 'ROLE_USER'])
    def results(String searchText) {
        def Annonce_list = Annonce.where {
            title =~ "%${searchText}%"}.list()
        return [Annonce_list: Annonce_list,
                searchText:searchText]

    }
}
