# GrailsProjetG10


Grails Projet, REST API
TP  - GRAILS

Projet : ETSENA


    Participants : GROUPE 10
    20 - RAHARINIVOSON Fehiniaina
    30 - RAMAHATSANGIARISON Hobiniaina Mirado
    37 - RANDRIAMANANA Nirina Rado
    38 - RANDRIAMANANTENASOA Sedera Nirina
    49 - RASOLOSON ANDRIAMIHAJAMALALA Henintsoa

Description
ETSENA est une application de gestion d’une société de dépôt de vente. D’une part, elle permet aux propriétaires de gérer les annonces publiées ainsi que les modérateurs de l’application. D’autre part, elle permet aux clients potentiels de parcourir les différents articles disponibles dans l’application.

    Prérequis d’environnement de développement pour l’intégration
    -   Java : version 1.8.281(jdk) ( ou une version qui accepte les requêtes HTTPS)
    -	Grails : version 3.3.8

Installation
Télécharger le projet (.zip), ou cloner le projet et lancer directement sur un IDE (IntelliJ)

Fonctionnalités:

I.	Partie Backend :

Tous ce qui est relatif à la partie gestion de l’application.
Toutes les méthodes décrites ci-après sont dans le fichier AnnonceController.groovy et UserController.groovy.

    1)	Gestion des annonces:
        a.	Affichage:
            Une fois chargé, les annonces sont affichées avec une pagination

        b.	Création d’une nouvelle annonce:
            Nous avons écrit une méthode afin de pouvoir créer une nouvelle annonce. Une méthode dans laquelle lorsqu’on soumet le formulaire, on vérifie les différentes données entrées et en cas de donnée manquante on retourne une erreur

        c.	Mise à jour ou modification: 
            L’application est également dotée d’un moyen qui lui permet d’effectuer une mise à jour des différentes annonces publiées

        d.	Suppression:
            La suppression fait partie des fonctionnalités nécessaires à la bonne gestion de l’application, donc elle y est intégrée

    2)	Gestion des utilisateurs
    
        Type d’utilisateur:
            o	Administrateur
                    Actions autorisées :
                    	- Création, suppression et modification des utilisateurs
                    	- Accès complet aux gestions des annonces
            o   Modérateur:
                    Actions autorisées :
                    	- Accès complet aux gestions des annonces
            o   Client
                    Action autorisé :
                    	- Parcourir les articles dans l’application



III.Partie API REST
La partie API REST répertorie les différents chemins pour avoir accès à différentes ressources nécessaire pour alimenter l’application.

    Installation:
        Prérequis:
         - Postman : Application qui permet de tester et d'explorer les Apis
         - Postman : ajout des 2 variables globals:
            * baseUrl: url de l'application (eg: http://localhost:8081
            * accessToken: token generer après l'authentification
         - Lancer le projet
        Dans la racine du projet , il existe un fichier GrailsProjectG10.postman_collection.json.
        1. Import du fichier
            Lancer l'application postman et importer ce fichier
        2. Authentification
            - Une fois importé, dérouler GrailsProjectG10 et vous aurez Authentification parmi la liste affiché
            - Cliqué sur Authentification
            - Envoyer la requête
            - Si utilisateur authentifié : ne pas oublié de modifier la valeur de accessToken si c'est pas encore renseigné

    Ci-après les différents URL selon les ressources qu’ils fournissent :
        Pour effectuer les tests vous pouvez lancer les différentes requêtes dans POSTMAN
        1.	Gestion des annonces :
            -   Création d'une annonce: POST {{baseUrl}}/api/annonces
            -   Annonces détaillées : GET  {{baseUrl}}/api/annonce/id
            -   Liste des annonces: GET {{baseUrl}}/api/annonces
            -   Modification partiel d'une annonce : PATCH {{baseUrl}}/api/annonce/id
            -   Modification ou mise à jour d'une annonce : PUT {{baseUrl}}/api/annonce/id
            -   Suppression d'une annonce: DEL {{baseUrl}}/api/annonce/id
        2.	Gestion des utilisateurs
            -   Création d'un utilisateur : POST {{baseUrl}}/api/user
            -   Profil utilisateur : GET {{baseUrl}}/api/user
            -   Liste des utilisateurs : GET {{baseUrl}}/api/users
            -   Modification ou mise à jour d'une annonce : PATCH {{baseUrl}}/api/user/id
            -   Suppression d'un utilisateur : DEL {{baseUrl}}/api/user/id




